`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//
// Create Date: 03.09.2020 22:18:45
// Design Name: Project SDUP
// Module Name: minmax
// Project Name: min_max_project
// Target Devices: Zybo
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// coeff - ax^3 + bx^2 + cx + d
// range - [lower, upper]

module degree3(
    input wire clk,
    input wire rst,
    input wire start,
    input wire signed [15:0]  a, b, c, d,
    input wire signed [15:0] lower, upper, //fixed point 10b.6b
    output reg               ready_out,
    output reg               overflow,
    output reg signed [31:0] min, max // fixed point 20b.12b
    );

    //states definition
    localparam D3_IDLE          = 5'b00000;
    localparam D3_UPPER         = 5'b00001;
    localparam D3_UPPER2        = 5'b00011;
    localparam D3_UPPER3        = 5'b00010;

    localparam D3_LOWER         = 5'b00110;
    localparam D3_LOWER2        = 5'b00111;
    localparam D3_LOWER3        = 5'b00101;
    localparam D3_CMPRANGE      = 5'b00100;

    localparam D3_XWDERIV_INIT  = 5'b01100;
    localparam D3_XWDERIV_CALC  = 5'b01101;
    localparam D3_YDERIV        = 5'b01111;
    localparam D3_Y2DERIV       = 5'b01110;

    localparam D3_DECIDE_DERIV  = 5'b01010;
    localparam D3_Y0            = 5'b01011;
    localparam D3_Y02           = 5'b01001;
    localparam D3_Y03           = 5'b01000;

    localparam D3_BISECTION     = 5'b11000;
    localparam D3_BISECTION2    = 5'b11001;
    localparam D3_BISECTION3    = 5'b11011;
    localparam D3_YZ            = 5'b11010;
    
    localparam D3_YZ2           = 5'b11110;
    localparam D3_YZ3           = 5'b11111;
    localparam D3_VIET          = 5'b11101;
    localparam D3_YZ1           = 5'b11100;
    
    localparam D3_YZ1_2         = 5'b10100;
    localparam D3_YZ1_3         = 5'b10101;
    localparam D3_COMPSEND      = 5'b10111;
    
    reg signed [31:0] min_nxt, max_nxt;
    reg ready_out_nxt;
    reg overflow_nxt;

    reg [4:0] state_d3, state_d3_nxt;
    
    wire signed [31:0] a_fix20b12b, b_fix20b12b, c_fix20b12b, d_fix20b12b, upper_fix20b12b, lower_fix20b12b;
    
    reg signed [31:0] mid_horner, mid_horner_nxt, mid_yw, mid_yw_nxt;
    reg signed [63:0] mid2_horner, mid2_horner_nxt;
    reg signed [63:0] res_upper, res_upper_nxt, res_lower, res_lower_nxt, res_yw, res_yw_nxt;
    reg signed [63:0] res_upper_deriv, res_upper_deriv_nxt, res_lower_deriv, res_lower_deriv_nxt;
    reg xw_in_range, xw_in_range_nxt;
    reg signed [31:0] xw, xw_nxt;
    reg signed [31:0] xz1, xz1_nxt, xz2, xz2_nxt;
    
    reg [63:0] div_b, div_b_nxt;
    reg [31:0] div_a, div_a_nxt;
    reg div_greater, div_greater_nxt;

    
    wire signed [16:0] twotimes_a;
    wire signed [17:0] threetimes_a;
    wire signed [31:0] twotimes_drv_a;
    
    wire signed [31:0] deriv_a;
    wire signed [32:0] deriv_b, deriv_c;
    reg [1:0] deriv_cntr, deriv_cntr_nxt;
    reg [5:0] bisec_cntr, bisec_cntr_nxt;
    reg [1:0] deriv_zeros_proc, deriv_zeros_proc_nxt;
    
    reg signed [63:0] bisec_up, bisec_yup, bisec_low, bisec_ylow, bisec_up_nxt, bisec_yup_nxt, bisec_low_nxt, bisec_ylow_nxt;
    reg signed [63:0] bisec_yz, bisec_yz_nxt;
    reg signed [63:0] res_eks1, res_eks1_nxt, res_eks2, res_eks2_nxt;   
    
    assign twotimes_a = a+a;
    assign threetimes_a = twotimes_a + {a[15],a};
    
    assign a_fix20b12b = {{10{a[15]}},a,6'b0};
    assign b_fix20b12b = {{10{b[15]}},b,6'b0};
    assign c_fix20b12b = {{10{c[15]}},c,6'b0};
    assign d_fix20b12b = {{10{d[15]}},d,6'b0};
    
    assign upper_fix20b12b = {{10{upper[15]}},upper,6'b0};
    assign lower_fix20b12b = {{10{lower[15]}},lower,6'b0};
    
    assign deriv_a = {{8{threetimes_a[17]}},threetimes_a,6'b0};
    assign twotimes_drv_a = deriv_a + deriv_a;
    assign deriv_b = b_fix20b12b<<1;
    assign deriv_c = c_fix20b12b;
    
    
    reg signed [31:0] mul1, mul2;
    reg signed [62:0] out_mul;
    always @* begin
        out_mul = mul1*mul2;
    end
    
    
 //**************FSM******************
    
    //sequential logic
    always @(posedge clk) begin
        if(rst == 1'b1) begin
            state_d3    <= D3_IDLE;
            min         <= 32'b0;
            max         <= 32'b0;
            ready_out   <= 1'b0;
            overflow    <= 1'b0;
            res_upper   <= 64'b0;
            res_lower   <= 64'b0;
            res_yw      <= 64'b0;
            mid_horner  <= 32'b0;
            mid_yw      <= 32'b0;
            mid2_horner <= 64'b0;
            xw          <= 32'b0;
            xz1         <= 32'b0;
            xz2         <= 32'b0;
            xw_in_range <= 1'b0;
            div_a       <= 32'b0;
            div_b       <= 64'b0;
            div_greater <= 1'b0;
            deriv_cntr  <= 2'b0;
            res_upper_deriv <= 64'b0;
            res_lower_deriv <= 64'b0;
            bisec_cntr  <= 6'b0;
            deriv_zeros_proc <= 2'b0;
            bisec_up    <= 64'b0;
            bisec_yup   <= 64'b0;
            bisec_low   <= 64'b0;
            bisec_ylow  <= 64'b0;
            bisec_yz    <= 64'b0;
            res_eks1    <= 64'b0;
            res_eks2    <= 64'b0;
        end
        else begin
            state_d3    <= state_d3_nxt;
            min         <= min_nxt;
            max         <= max_nxt;
            ready_out   <= ready_out_nxt;
            overflow    <= overflow_nxt;
            res_upper   <= res_upper_nxt;
            res_lower   <= res_lower_nxt;
            res_yw      <= res_yw_nxt;
            mid_horner  <= mid_horner_nxt;
            mid_yw      <= mid_yw_nxt;
            mid2_horner <= mid2_horner_nxt;
            xw          <= xw_nxt;
            xz1         <= xz1_nxt;
            xz2         <= xz2_nxt;
            xw_in_range <= xw_in_range_nxt;
            div_a       <= div_a_nxt;
            div_b       <= div_b_nxt;
            div_greater <= div_greater_nxt;
            deriv_cntr  <= deriv_cntr_nxt;
            res_upper_deriv <= res_upper_deriv_nxt;
            res_lower_deriv <= res_lower_deriv_nxt;
            bisec_cntr  <= bisec_cntr_nxt;
            deriv_zeros_proc <= deriv_zeros_proc_nxt;
            bisec_up    <= bisec_up_nxt;  
            bisec_yup   <= bisec_yup_nxt; 
            bisec_low   <= bisec_low_nxt;
            bisec_ylow  <= bisec_ylow_nxt;
            bisec_yz    <= bisec_yz_nxt;
            res_eks1    <= res_eks1_nxt;
            res_eks2    <= res_eks2_nxt;
        end
    end
    
    // next state logic
    always @* begin 
        if(rst==1'b1) begin
            state_d3_nxt = D3_IDLE;
        end
        else begin
            casex({state_d3,        start, xw_in_range, div_greater, deriv_cntr, deriv_zeros_proc, bisec_cntr})
                  {D3_IDLE,         1'b0,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_IDLE;
                  {D3_IDLE,         1'b1,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_UPPER;
                  {D3_UPPER,        1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_UPPER2;
                  {D3_UPPER2,       1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_UPPER3;
                  {D3_UPPER3,       1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_LOWER;
                  {D3_LOWER,        1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_LOWER2;
                  {D3_LOWER2,       1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_LOWER3;
                  {D3_LOWER3,       1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_CMPRANGE;
                  {D3_CMPRANGE,     1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_XWDERIV_INIT;
                  {D3_XWDERIV_INIT, 1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_XWDERIV_CALC;
                  {D3_XWDERIV_CALC, 1'bx,  1'bx,        1'b1,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_XWDERIV_CALC;
                  {D3_XWDERIV_CALC, 1'bx,  1'bx,        1'b0,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_YDERIV;
                  {D3_YDERIV,       1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_Y2DERIV;
                  {D3_Y2DERIV,      1'bx,  1'bx,        1'bx,        2'b0x,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_YDERIV;
                  {D3_Y2DERIV,      1'bx,  1'bx,        1'bx,        2'b10,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_YDERIV;
                  {D3_Y2DERIV,      1'bx,  1'bx,        1'bx,        2'b11,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_DECIDE_DERIV;
                  {D3_DECIDE_DERIV, 1'bx,  1'bx,        1'bx,        2'bxx,      2'b00,            6'bxxxxxx } : state_d3_nxt = D3_COMPSEND;
                  {D3_DECIDE_DERIV, 1'bx,  1'bx,        1'bx,        2'bxx,      2'b01,            6'bxxxxxx } : state_d3_nxt = D3_Y0;
                  {D3_DECIDE_DERIV, 1'bx,  1'bx,        1'bx,        2'bxx,      2'b1x,            6'bxxxxxx } : state_d3_nxt = D3_BISECTION;
                  {D3_Y0,           1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_Y02;
                  {D3_Y02,          1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_Y03;
                  {D3_Y03,          1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_COMPSEND;
                  {D3_BISECTION,    1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_BISECTION2;
                  {D3_BISECTION2,   1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_BISECTION3;
                  {D3_BISECTION3,   1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'b0xxxxx } : state_d3_nxt = D3_BISECTION;
                  {D3_BISECTION3,   1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'b1xxxxx } : state_d3_nxt = D3_YZ;
                  {D3_YZ,           1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_YZ2;
                  {D3_YZ2,          1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_YZ3;
                  {D3_YZ3,          1'bx,  1'bx,        1'bx,        2'bxx,      2'b0x,            6'bxxxxxx } : state_d3_nxt = D3_COMPSEND;
                  {D3_YZ3,          1'bx,  1'bx,        1'bx,        2'bxx,      2'b10,            6'bxxxxxx } : state_d3_nxt = D3_COMPSEND;
                  {D3_YZ3,          1'bx,  1'bx,        1'bx,        2'bxx,      2'b11,            6'bxxxxxx } : state_d3_nxt = D3_VIET;
                  {D3_VIET,         1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_YZ1;
                  {D3_YZ1,          1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_YZ1_2;
                  {D3_YZ1_2,        1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_YZ1_3;
                  {D3_YZ1_3,        1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_COMPSEND;
                  {D3_COMPSEND,     1'bx,  1'bx,        1'bx,        2'bxx,      2'bxx,            6'bxxxxxx } : state_d3_nxt = D3_IDLE;
                  default: state_d3_nxt = state_d3; 
            endcase
        end
    end
    
    //combinational logic
    always @* begin
        if(rst == 1'b1) begin
            min_nxt = 32'b0;       
            max_nxt = 32'b0;       
            ready_out_nxt   = 1'b0;
            overflow_nxt    = 1'b0;
            res_upper_nxt   = 64'b0;
            res_lower_nxt   = 64'b0;
            res_yw_nxt      = 64'b0;
            mid_horner_nxt  = 32'b0;
            mid_yw_nxt      = 32'b0;
            mid2_horner_nxt = 64'b0;
            xw_nxt          = 32'b0;
            xz1_nxt         = 32'b0;
            xz2_nxt         = 32'b0;
            xw_in_range_nxt = 1'b0;
            div_a_nxt       = 32'b0;
            div_b_nxt       = 64'b0;
            div_greater_nxt = 1'b0;
            deriv_cntr_nxt  = 2'b0;
            res_upper_deriv_nxt = 64'b0;
            res_lower_deriv_nxt = 64'b0;
            bisec_cntr_nxt  = 6'b0;
            deriv_zeros_proc_nxt = 2'b0;
            bisec_up_nxt    = 64'b0;
            bisec_yup_nxt   = 64'b0;
            bisec_low_nxt   = 64'b0;
            bisec_ylow_nxt  = 64'b0;
            bisec_yz_nxt    = 64'b0;
            res_eks1_nxt    = 64'b0;
            res_eks2_nxt    = 64'b0;
            mul1 = 32'b0;
            mul2 = 32'b0;  
        end
        else begin
            min_nxt = 32'b0;       
            max_nxt = 32'b0;       
            ready_out_nxt   = 1'b0;
            overflow_nxt    = 1'b0;
            res_upper_nxt   = 64'b0;
            res_lower_nxt   = 64'b0;
            res_yw_nxt      = 64'b0;
            mid_horner_nxt  = 32'b0;
            mid_yw_nxt      = 32'b0;
            mid2_horner_nxt = 64'b0;
            xw_nxt          = 32'b0;
            xz1_nxt         = 32'b0;
            xz2_nxt         = 32'b0;
            xw_in_range_nxt = 1'b0;
            div_a_nxt       = 32'b0;
            div_b_nxt       = 64'b0;
            div_greater_nxt = 1'b0;
            deriv_cntr_nxt  = 2'b0;
            res_upper_deriv_nxt = 64'b0;
            res_lower_deriv_nxt = 64'b0;
            bisec_cntr_nxt  = 6'b0;
            deriv_zeros_proc_nxt = 2'b0;
            bisec_up_nxt    = 64'b0;
            bisec_yup_nxt   = 64'b0;
            bisec_low_nxt   = 64'b0;
            bisec_ylow_nxt  = 64'b0;
            bisec_yz_nxt    = 64'b0;
            res_eks1_nxt    = 64'b0;
            res_eks2_nxt    = 64'b0;
            mul1 = 32'b0;
            mul2 = 32'b0;  
                  
            case(state_d3_nxt)
                D3_IDLE: begin
                    min_nxt = min;     
                    max_nxt = max;
                    ready_out_nxt = ready_out;
                    overflow_nxt = overflow;
                end
                
                D3_UPPER: begin
                    mid_horner_nxt = (a * upper) + b_fix20b12b;
                end
                
                D3_UPPER2: begin
                    mid2_horner_nxt = ((mid_horner*upper_fix20b12b)>>>12) + c_fix20b12b;
                end
                
                D3_UPPER3: begin
                    res_upper_nxt = ((mid2_horner*upper_fix20b12b)>>>12) + d_fix20b12b;
                end
                
                D3_LOWER: begin
                    mid_horner_nxt = (a * lower) + b_fix20b12b;
                    res_upper_nxt = res_upper;
                end
                 
                D3_LOWER2: begin
                    mid2_horner_nxt = ((mid_horner*lower_fix20b12b)>>>12) + c_fix20b12b;
                    res_upper_nxt = res_upper;
                end
                 
                D3_LOWER3: begin
                    res_lower_nxt = ((mid2_horner*lower_fix20b12b)>>>12) + d_fix20b12b;
                    res_upper_nxt = res_upper;
                end
                
                D3_CMPRANGE: begin
                    if(res_upper > res_lower) begin
                        max_nxt = res_upper[31:0];
                        min_nxt = res_lower[31:0];
                    end                    
                    else begin
                        max_nxt = res_lower[31:0];
                        min_nxt = res_upper[31:0];
                    end                  
                    overflow_nxt = ((&res_lower[63:31])^(|res_lower[63:31])) | ((&res_upper[63:31])^(|res_upper[63:31]));
                end
                
                D3_XWDERIV_INIT: begin
                    div_b_nxt = ((deriv_b[32]==1'b1)? ({31'b0,~deriv_b[32:0]})+ 1'b1 : {31'b0,deriv_b})<<12;
                    div_a_nxt = (twotimes_drv_a[31]==1'b1)? (~twotimes_drv_a[31:0])+ 1'b1 : twotimes_drv_a; 
                    max_nxt = max;
                    min_nxt = min;    
                    overflow_nxt = overflow;            
                end
                
                D3_XWDERIV_CALC: begin
                    if(div_b >= div_a) begin
                        div_b_nxt = div_b - div_a;
                        div_a_nxt = div_a;
                        xw_nxt = xw + 1;
                        div_greater_nxt = 1'b1;
                    end
                    else begin
                        xw_nxt = ((deriv_a<0 && deriv_b<0) || (deriv_a>0 && deriv_b>0))? -xw : xw;
                        xw_in_range_nxt = (xw_nxt>lower_fix20b12b && xw_nxt<upper_fix20b12b)? 1'b1 : 1'b0;
                    end
                    
                    max_nxt = max;
                    min_nxt = min; 
                    overflow_nxt = overflow;            
                end
                
                D3_YDERIV: begin
                    case(deriv_cntr)
                        2'b00: begin
                            mul1 = deriv_a;
                            mul2 = xw;
                            mid_yw_nxt = ((out_mul)>>>12) + deriv_b;
                        end
                        2'b01: begin
                            mul1 = deriv_a;
                            mul2 = upper_fix20b12b;
                            mid_yw_nxt = ((out_mul)>>>12) + deriv_b;
                        end
                        2'b10: begin
                            mul1 = deriv_a;
                            mul2 = lower_fix20b12b;
                            mid_yw_nxt = ((out_mul)>>>12) + deriv_b;
                            
                        end
                        default: begin
                            mid_yw_nxt = 31'b0;
                            mul1 = 32'b0;
                            mul2 = 32'b0;
                        end
                    endcase
                    
                    res_yw_nxt = res_yw;
                    res_upper_deriv_nxt = res_upper_deriv;
                    res_lower_deriv_nxt = res_lower_deriv_nxt;
                    deriv_cntr_nxt = deriv_cntr;
                    
                    xw_nxt = xw;
                    xw_in_range_nxt = xw_in_range;
                    max_nxt = max;
                    min_nxt = min; 
                    overflow_nxt = overflow;            
                end
                
                D3_Y2DERIV: begin
                    case(deriv_cntr)
                        2'b00: begin
                            mul1 = mid_yw;
                            mul2 = xw;
                            res_yw_nxt = ((out_mul)>>>12) + deriv_c;
                            res_upper_deriv_nxt = res_upper_deriv;
                            res_lower_deriv_nxt = res_lower_deriv_nxt;
                            deriv_cntr_nxt = deriv_cntr + 1'b1;
                        end
                        2'b01: begin
                            mul1 = mid_yw;
                            mul2 = upper_fix20b12b;
                            res_upper_deriv_nxt = ((out_mul)>>>12) + deriv_c;
                            res_yw_nxt = res_yw;
                            res_lower_deriv_nxt = res_lower_deriv;
                            deriv_cntr_nxt = deriv_cntr + 1'b1;
                        end
                        2'b10: begin
                            mul1 = mid_yw;
                            mul2 = lower_fix20b12b;
                            res_lower_deriv_nxt = ((out_mul)>>>12) + deriv_c;
                            res_yw_nxt = res_yw;
                            res_upper_deriv_nxt = res_upper_deriv;
                            deriv_cntr_nxt = deriv_cntr + 1'b1;
                        end
                        default: begin
                            res_yw_nxt = 64'b0;
                            res_upper_deriv_nxt = 64'b0;
                            res_lower_deriv_nxt = 64'b0;
                            deriv_cntr_nxt = 1'b0;
                            mul1 = 32'b0;
                            mul2 = 32'b0;
                        end
                    endcase
                    xw_nxt = xw;
                    xw_in_range_nxt = xw_in_range;
                    max_nxt = max;
                    min_nxt = min;
                    overflow_nxt = overflow;            
                end
                
                D3_DECIDE_DERIV: begin
                    if((res_yw == 64'b0) && (xw_in_range==1'b1)) begin 
                        deriv_zeros_proc_nxt = 2'b01;
                    end
                    else if((res_yw > 0 && deriv_a < 0) || (res_yw < 0 && deriv_a > 0)) begin
                        if(xw_in_range==1'b1) begin
                            if(((res_yw > 0 && res_lower_deriv < 0) || (res_yw < 0 && res_lower_deriv > 0)) && ((res_yw > 0 && res_upper_deriv < 0) || (res_yw < 0 && res_upper_deriv > 0))) begin
                                deriv_zeros_proc_nxt = 2'b11;
                                bisec_up_nxt = xw;
                                bisec_yup_nxt = res_yw;
                                bisec_low_nxt = lower_fix20b12b;
                                bisec_ylow_nxt = res_lower_deriv;
                            end                    
                            else if ((res_yw > 0 && res_lower_deriv < 0) || (res_yw < 0 && res_lower_deriv > 0)) begin
                                deriv_zeros_proc_nxt = 2'b10;
                                bisec_up_nxt = xw;
                                bisec_yup_nxt = res_yw;
                                bisec_low_nxt = lower_fix20b12b;
                                bisec_ylow_nxt = res_lower_deriv;
        
                            end
                            else if ((res_yw > 0 && res_upper_deriv < 0) || (res_yw < 0 && res_upper_deriv > 0)) begin
                                deriv_zeros_proc_nxt = 2'b10;
                                bisec_low_nxt = xw;
                                bisec_ylow_nxt = res_yw;
                                bisec_up_nxt = upper_fix20b12b;
                                bisec_yup_nxt = res_upper_deriv;
        
                            end
                            else begin
                                deriv_zeros_proc_nxt = 2'b00;
                            end
                        end
                        else if ((res_upper_deriv > 0 && res_lower_deriv < 0) || (res_upper_deriv < 0 && res_lower_deriv > 0)) begin
                                deriv_zeros_proc_nxt = 2'b10;
                                bisec_low_nxt = lower_fix20b12b;
                                bisec_ylow_nxt = res_lower_deriv;
                                bisec_up_nxt = upper_fix20b12b;
                                bisec_yup_nxt = res_upper_deriv;
                        end
                        else begin
                                deriv_zeros_proc_nxt = 2'b00;
                        end
                        
                    end
                    else begin
                        deriv_zeros_proc_nxt = 2'b00;
                    end
                    xw_nxt = xw;
                    xw_in_range_nxt = xw_in_range;
                    max_nxt = max;
                    min_nxt = min;
                    overflow_nxt = overflow;            
                end
                
                D3_BISECTION: begin
                    bisec_cntr_nxt = bisec_cntr;
                    xz1_nxt = (bisec_low + bisec_up)>>1;
                    mul1 = deriv_a;
                    mul2 = xz1_nxt;
                    bisec_yz_nxt = ((out_mul)>>>12) + deriv_b;
                                        
                    bisec_low_nxt  = bisec_low;
                    bisec_ylow_nxt = bisec_ylow;
                    bisec_up_nxt   = bisec_up;
                    bisec_yup_nxt  = bisec_yup;
                    deriv_zeros_proc_nxt = deriv_zeros_proc;
                    xw_nxt = xw;
                    xw_in_range_nxt = xw_in_range;
                    max_nxt = max;
                    min_nxt = min;
                    overflow_nxt = overflow;            
                end
                D3_BISECTION2: begin
                    bisec_cntr_nxt = bisec_cntr;
                    xz1_nxt = xz1;
                    mul1 = bisec_yz;
                    mul2 = xz1;
                    bisec_yz_nxt = ((out_mul)>>>12) + deriv_c;
                    
                    bisec_low_nxt  = bisec_low;
                    bisec_ylow_nxt = bisec_ylow;
                    bisec_up_nxt   = bisec_up;
                    bisec_yup_nxt  = bisec_yup;
                    deriv_zeros_proc_nxt = deriv_zeros_proc;
                    xw_nxt = xw;
                    xw_in_range_nxt = xw_in_range;
                    max_nxt = max;
                    min_nxt = min;
                    overflow_nxt = overflow;            
                end
                
                D3_BISECTION3: begin
                    bisec_cntr_nxt = bisec_cntr + 1'b1;
                    xz1_nxt = xz1;
                    
                    if((bisec_yz < 0 && bisec_ylow >0) || (bisec_yz > 0 && bisec_ylow <0)) begin
                        bisec_low_nxt  = bisec_low;
                        bisec_ylow_nxt = bisec_ylow;
                        bisec_up_nxt   = xz1;
                        bisec_yup_nxt  = bisec_yz;
                    end
                    else if((bisec_yz < 0 && bisec_yup >0) || (bisec_yz > 0 && bisec_yup <0)) begin
                        bisec_low_nxt  = xz1;
                        bisec_ylow_nxt = bisec_yz;
                        bisec_up_nxt   = bisec_up;
                        bisec_yup_nxt  = bisec_yup;
                    end
                    else begin
                        bisec_low_nxt  = bisec_low;
                        bisec_ylow_nxt = bisec_ylow;
                        bisec_up_nxt   = bisec_up;
                        bisec_yup_nxt  = bisec_yup;
                    end
                    
                    deriv_zeros_proc_nxt = deriv_zeros_proc;
                    xw_nxt = xw;
                    xw_in_range_nxt = xw_in_range;
                    max_nxt = max;
                    min_nxt = min;
                    overflow_nxt = overflow;            
                end
                
                D3_YZ: begin
                    mid_horner_nxt = ((a_fix20b12b * xz1)>>>12) + b_fix20b12b;
                    deriv_zeros_proc_nxt = deriv_zeros_proc;
                    xz1_nxt = xz1;
                    xw_nxt = xw;
                    xw_in_range_nxt = xw_in_range;
                    max_nxt = max;
                    min_nxt = min;
                    overflow_nxt = overflow;            
                end
                 
                D3_YZ2: begin
                    mid2_horner_nxt = ((mid_horner*xz1)>>>12) + c_fix20b12b;
                    deriv_zeros_proc_nxt = deriv_zeros_proc;
                    xz1_nxt = xz1;
                    xw_nxt = xw;
                    xw_in_range_nxt = xw_in_range;
                    max_nxt = max;
                    min_nxt = min;
                    overflow_nxt = overflow;            
                end
                 
                D3_YZ3: begin
                    res_eks1_nxt = ((mid2_horner*xz1)>>>12) + d_fix20b12b;
                    deriv_zeros_proc_nxt = deriv_zeros_proc;
                    xz1_nxt = xz1;
                    xw_nxt = xw;
                    xw_in_range_nxt = xw_in_range;
                    max_nxt = max;
                    min_nxt = min;
                    overflow_nxt = overflow;            
                end
                
                D3_VIET: begin
                    xz2_nxt = (xw<<1) - xz1;
                    res_eks1_nxt = res_eks1;
                    deriv_zeros_proc_nxt = deriv_zeros_proc;
                    xw_nxt = xw;
                    xw_in_range_nxt = xw_in_range;
                    max_nxt = max;
                    min_nxt = min;
                    overflow_nxt = overflow;            
                end
                D3_YZ1: begin
                    mid_horner_nxt = ((a_fix20b12b * xz2)>>>12) + b_fix20b12b;
                    res_eks1_nxt = res_eks1;
                    deriv_zeros_proc_nxt = deriv_zeros_proc;
                    xw_nxt = xw;
                    xz2_nxt = xz2;
                    xw_in_range_nxt = xw_in_range;
                    max_nxt = max;
                    min_nxt = min;
                    overflow_nxt = overflow;            
                end
                 
                D3_YZ1_2: begin
                    mid2_horner_nxt = ((mid_horner*xz2)>>>12) + c_fix20b12b;
                    res_eks1_nxt = res_eks1;
                    deriv_zeros_proc_nxt = deriv_zeros_proc;
                    xw_nxt = xw;
                    xz2_nxt = xz2;
                    xw_in_range_nxt = xw_in_range;
                    max_nxt = max;
                    min_nxt = min;
                    overflow_nxt = overflow;            
                end
                 
                D3_YZ1_3: begin
                    res_eks2_nxt = ((mid2_horner*xz2)>>>12) + d_fix20b12b;
                    res_eks1_nxt = res_eks1;
                    deriv_zeros_proc_nxt = deriv_zeros_proc;
                    xw_nxt = xw;
                    xz2_nxt = xz2;
                    xw_in_range_nxt = xw_in_range;
                    max_nxt = max;
                    min_nxt = min;
                    overflow_nxt = overflow;            
                end

                D3_Y0: begin
                    mid_horner_nxt = ((a_fix20b12b * xw)>>>12) + b_fix20b12b;
                    deriv_zeros_proc_nxt = deriv_zeros_proc;
                    xw_nxt = xw;
                    xw_in_range_nxt = xw_in_range;
                    max_nxt = max;
                    min_nxt = min;
                    overflow_nxt = overflow;            
                end
                 
                D3_Y02: begin
                    mid2_horner_nxt = ((mid_horner*xw)>>>12) + c_fix20b12b;
                    deriv_zeros_proc_nxt = deriv_zeros_proc;
                    xw_nxt = xw;
                    xw_in_range_nxt = xw_in_range;
                    max_nxt = max;
                    min_nxt = min;
                    overflow_nxt = overflow;            
                end
                 
                D3_Y03: begin
                    res_eks1_nxt = ((mid2_horner*xw)>>>12) + d_fix20b12b;
                    deriv_zeros_proc_nxt = deriv_zeros_proc;
                    xw_nxt = xw;
                    xw_in_range_nxt = xw_in_range;
                    max_nxt = max;
                    min_nxt = min;
                    overflow_nxt = overflow;            
                end
                
                D3_COMPSEND: begin
                    if(deriv_zeros_proc == 2'b00) begin 
                        max_nxt = max;
                        min_nxt = min;
                    end
                    else if(deriv_zeros_proc == 2'b01 || deriv_zeros_proc == 2'b10) begin 
                        if(res_eks1 > max) begin
                            max_nxt = res_eks1[31:0];
                            min_nxt = min;
                        end                    
                        else if (res_eks1 < min) begin
                            max_nxt = max;
                            min_nxt = res_eks1[31:0];
                        end
                        else begin
                            max_nxt = max;
                            min_nxt = min;
                        end
                    end
                    else begin
                        max_nxt = max;
                        min_nxt = min;
                        if(res_eks1 > max) begin
                            max_nxt = res_eks1[31:0];
                        end                    
                        else if (res_eks1 < min) begin
                            min_nxt = res_eks1[31:0];
                        end
                        else begin
                            max_nxt = max;
                            min_nxt = min;
                        end
                        
                        if(res_eks2 > max) begin
                            max_nxt = res_eks2[31:0];
                        end                    
                        else if (res_eks2 < min) begin
                            min_nxt = res_eks2[31:0];
                        end
                        else begin
                        end
                    end
                    overflow_nxt = overflow | ((&res_eks1[63:31])^(|res_eks1[63:31])) | ((&res_eks2[63:31])^(|res_eks2[63:31])); ;            
                    ready_out_nxt = 1'b1;                    
                end
                
                default: begin
                    min_nxt = 32'b0;       
                    max_nxt = 32'b0;       
                    ready_out_nxt   = 1'b0;
                    overflow_nxt    = 1'b0;
                    res_upper_nxt   = 64'b0;
                    res_lower_nxt   = 64'b0;
                    res_yw_nxt      = 64'b0;
                    mid_horner_nxt  = 32'b0;
                    mid_yw_nxt      = 32'b0;
                    mid2_horner_nxt = 64'b0;
                    xw_nxt          = 32'b0;
                    xz1_nxt         = 32'b0;
                    xz2_nxt         = 32'b0;
                    xw_in_range_nxt = 1'b0;
                    div_a_nxt       = 32'b0;
                    div_b_nxt       = 64'b0;
                    deriv_cntr_nxt  = 2'b0;
                    res_upper_deriv_nxt = 64'b0;
                    res_lower_deriv_nxt = 64'b0;
                    bisec_cntr_nxt  = 6'b0;
                    deriv_zeros_proc_nxt = 2'b0;
                    bisec_up_nxt    = 64'b0;
                    bisec_yup_nxt   = 64'b0;
                    bisec_low_nxt   = 64'b0;
                    bisec_ylow_nxt  = 64'b0;
                    bisec_yz_nxt    = 64'b0;
                    res_eks1_nxt    = 64'b0;
                    res_eks2_nxt    = 64'b0;
                    mul1 = 32'b0;
                    mul2 = 32'b0;  
                end
            endcase
        end
    end
    
endmodule
