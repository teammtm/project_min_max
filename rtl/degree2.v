`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//
// Create Date: 03.09.2020 22:18:45
// Design Name: Project SDUP
// Module Name: minmax
// Project Name: min_max_project
// Target Devices: Zybo
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// coeff - ax^2 + bx + c
// range - [lower, upper]

module degree2(
    input wire clk,
    input wire rst,
    input wire start,
    input wire signed [15:0] a, b, c, //fixed point 10b.6b
    input wire signed [15:0] lower, upper, //fixed point 10b.6b
    output reg               ready_out,
    output reg               overflow,
    output reg signed [31:0] min, max // fixed point 20b.12b
    );
    
    //states definition
    localparam D2_IDLE     = 4'b0000;
    localparam D2_UPPER    = 4'b0001;
    localparam D2_UPPER2   = 4'b0011;
    localparam D2_LOWER    = 4'b0010;
    localparam D2_LOWER2   = 4'b0110;
    localparam D2_CMPRANGE = 4'b0111;
    localparam D2_XW_INIT  = 4'b0101;
    localparam D2_XW_CALC  = 4'b0100;
    localparam D2_YW       = 4'b1100;
    localparam D2_YW2      = 4'b1101;
    localparam D2_COMPSEND = 4'b1111;
    
    reg signed [31:0] min_nxt, max_nxt;
    reg ready_out_nxt;
    reg overflow_nxt;
    reg [3:0] state_d2, state_d2_nxt;
    wire signed [31:0] a_fix20b12b, b_fix20b12b, c_fix20b12b, upper_fix20b12b, lower_fix20b12b;
    reg signed [31:0] mid_horner, mid_horner_nxt;
    reg signed [63:0] mid_ywhorner, mid_ywhorner_nxt;
    reg signed [63:0] res_upper, res_upper_nxt, res_lower, res_lower_nxt, res_yw, res_yw_nxt;
    reg xw_in_range, xw_in_range_nxt;
    reg signed [31:0] xw, xw_nxt;
    reg [63:0] div_b, div_b_nxt;
    reg [31:0] div_a, div_a_nxt;
    reg div_greater, div_greater_nxt;
    wire signed[16:0] twotimes_a;
    
    assign twotimes_a = {a[15],a}+{a[15],a};
    assign a_fix20b12b = {{10{a[15]}},a,6'b0};
    assign b_fix20b12b = {{10{b[15]}},b,6'b0};
    assign c_fix20b12b = {{10{c[15]}},c,6'b0};
    assign upper_fix20b12b = {{10{upper[15]}},upper,6'b0};
    assign lower_fix20b12b = {{10{lower[15]}},lower,6'b0};
    
 //**************FSM******************
    
    //sequential logic
    always @(posedge clk) begin
        if(rst == 1'b1) begin
            state_d2    <= D2_IDLE;
            min         <= 32'b0;
            max         <= 32'b0;
            ready_out   <= 1'b0;
            res_upper   <= 64'b0;
            res_lower   <= 64'b0;
            res_yw      <= 64'b0;
            mid_horner  <= 32'b0;
            mid_ywhorner <= 64'b0;
            xw          <= 32'b0;
            xw_in_range <= 1'b0;
            div_a       <= 32'b0;
            div_b       <= 64'b0;
            overflow    <= 1'b0;
            div_greater <= 1'b0;
        end
        else begin
            state_d2    <= state_d2_nxt;
            min         <= min_nxt;
            max         <= max_nxt;
            ready_out   <= ready_out_nxt;
            overflow    <= overflow_nxt;
            res_upper   <= res_upper_nxt;
            res_lower   <= res_lower_nxt;
            res_yw      <= res_yw_nxt;
            mid_horner  <= mid_horner_nxt;
            mid_ywhorner <= mid_ywhorner_nxt;
            xw          <= xw_nxt;
            xw_in_range <= xw_in_range_nxt;            
            div_a       <= div_a_nxt;
            div_b       <= div_b_nxt;
            div_greater <= div_greater_nxt;
        end
    end
    
    // next state logic
    always @* begin 
        if(rst==1'b1) begin
            state_d2_nxt = D2_IDLE;
        end
        else begin
            casex({state_d2,    start, xw_in_range, div_greater})
                  {D2_IDLE,     1'b0,  1'bx,        1'bx} : state_d2_nxt = D2_IDLE;
                  {D2_IDLE,     1'b1,  1'bx,        1'bx} : state_d2_nxt = D2_UPPER;
                  {D2_UPPER,    1'bx,  1'bx,        1'bx} : state_d2_nxt = D2_UPPER2;
                  {D2_UPPER2,   1'bx,  1'bx,        1'bx} : state_d2_nxt = D2_LOWER;
                  {D2_LOWER,    1'bx,  1'bx,        1'bx} : state_d2_nxt = D2_LOWER2;
                  {D2_LOWER2,   1'bx,  1'bx,        1'bx} : state_d2_nxt = D2_CMPRANGE;
                  {D2_CMPRANGE, 1'bx,  1'bx,        1'bx} : state_d2_nxt = D2_XW_INIT;
                  {D2_XW_INIT,  1'bx,  1'bx,        1'bx} : state_d2_nxt = D2_XW_CALC;
                  {D2_XW_CALC,  1'bx,  1'bx,        1'b1} : state_d2_nxt = D2_XW_CALC;
                  {D2_XW_CALC,  1'bx,  1'b0,        1'b0} : state_d2_nxt = D2_COMPSEND;
                  {D2_XW_CALC,  1'bx,  1'b1,        1'b0} : state_d2_nxt = D2_YW;
                  {D2_YW,       1'bx,  1'bx,        1'bx} : state_d2_nxt = D2_YW2;
                  {D2_YW2,      1'bx,  1'bx,        1'bx} : state_d2_nxt = D2_COMPSEND;
                  {D2_COMPSEND, 1'bx,  1'bx,        1'bx} : state_d2_nxt = D2_IDLE;
                  default: state_d2_nxt = state_d2;
            endcase
        end
    end
    
    //combinational logic
    always @* begin
        if(rst == 1'b1) begin
            min_nxt = 32'b0;       
            max_nxt = 32'b0;       
            ready_out_nxt = 1'b0;
            overflow_nxt  = 1'b0;
            res_upper_nxt = 64'b0;
            res_lower_nxt = 64'b0;
            res_yw_nxt    = 64'b0;
            mid_horner_nxt = 32'b0;
            mid_ywhorner_nxt = 64'b0;
            xw_nxt = 32'b0;
            xw_in_range_nxt = 1'b0;
            div_a_nxt  = 32'b0;
            div_b_nxt  = 64'b0;
            div_greater_nxt = 1'b0;
        end
        else begin
            min_nxt = 32'b0;       
            max_nxt = 32'b0;       
            ready_out_nxt = 1'b0;
            overflow_nxt  = 1'b0;
            res_upper_nxt = 64'b0;
            res_lower_nxt = 64'b0;
            res_yw_nxt    = 64'b0;
            mid_horner_nxt = 32'b0;
            mid_ywhorner_nxt = 64'b0;
            xw_nxt = 32'b0;
            xw_in_range_nxt = 1'b0;
            div_a_nxt  = 32'b0;
            div_b_nxt  = 64'b0;
            div_greater_nxt = 1'b0;

            case(state_d2_nxt)
            
                D2_IDLE: begin
                    min_nxt = min;     
                    max_nxt = max;
                    ready_out_nxt = ready_out;
                    overflow_nxt = overflow;
                end
                
                D2_UPPER: begin
                    mid_horner_nxt = (a * upper) + b_fix20b12b;
                end
                
                D2_UPPER2: begin
                    res_upper_nxt = ((mid_horner*upper_fix20b12b)>>>12) + c_fix20b12b;
                end
                
                D2_LOWER: begin
                    mid_horner_nxt = (a * lower) + b_fix20b12b;
                    res_upper_nxt = res_upper;
                end
                 
                D2_LOWER2: begin
                    res_lower_nxt = ((mid_horner*lower_fix20b12b)>>>12) + c_fix20b12b;
                    res_upper_nxt = res_upper;
                end
                
                D2_CMPRANGE: begin
                    if(res_upper > res_lower) begin
                        max_nxt = res_upper[31:0];
                        min_nxt = res_lower[31:0];
                    end                    
                    else begin
                        max_nxt = res_lower[31:0];
                        min_nxt = res_upper[31:0];
                    end       
                    overflow_nxt = ((&res_lower[63:31])^(|res_lower[63:31])) | ((&res_upper[63:31])^(|res_upper[63:31]));
                end
                
                D2_XW_INIT: begin
                    div_b_nxt = ((b_fix20b12b[31]==1'b1)? ({32'b0,~b_fix20b12b[31:0]})+ 1'b1 : {31'b0,b_fix20b12b})<<12;
                    div_a_nxt = (twotimes_a[16]==1'b1)? {9'b0,(~twotimes_a[16:0])+ 1'b1,6'b0} : {9'b0,twotimes_a,6'b0}; 
                    max_nxt = max;
                    min_nxt = min;
                    overflow_nxt = overflow;            
                end
                
                D2_XW_CALC: begin
                    if(div_b >= div_a) begin
                        div_b_nxt = div_b - div_a;
                        div_a_nxt = div_a;
                        xw_nxt = xw + 1;
                        div_greater_nxt = 1'b1;
                    end
                    else begin
                        xw_nxt = ((a<0 && b<0) || (a>0 && b>0))? -xw : xw;
                        xw_in_range_nxt = (xw_nxt>lower_fix20b12b && xw_nxt<upper_fix20b12b)? 1'b1 : 1'b0;
                        div_greater_nxt = 1'b0;
                    end
                    
                    max_nxt = max;
                    min_nxt = min; 
                    overflow_nxt = overflow;
                end
                
                D2_YW: begin
                    mid_ywhorner_nxt = ((a_fix20b12b * xw)>>>12) + b_fix20b12b;
                    xw_nxt = xw;
                    max_nxt = max;
                    min_nxt = min; 
                    xw_in_range_nxt = xw_in_range;
                    overflow_nxt = overflow;
                end
                
                D2_YW2: begin
                    res_yw_nxt = ((mid_ywhorner*xw)>>>12) + c_fix20b12b;
                    max_nxt = max;
                    min_nxt = min;
                    xw_in_range_nxt = xw_in_range;
                    overflow_nxt = overflow;
                end
                
                D2_COMPSEND: begin
                    if(xw_in_range == 1'b0) begin 
                        max_nxt = max;
                        min_nxt = min;
                    end
                    else begin
                        if(res_yw > max) begin
                            max_nxt = res_yw;
                            min_nxt = min;
                        end                    
                        else if (res_yw < min) begin
                            max_nxt = max;
                            min_nxt = res_yw;
                        end
                        else begin
                            max_nxt = max;
                            min_nxt = min;
                        end
                    end
                    
                    overflow_nxt = overflow | ((&res_yw[63:31])^(|res_yw[63:31]));
                    ready_out_nxt = 1'b1;                    
                end
                
                default: begin
                    min_nxt = 32'b0;       
                    max_nxt = 32'b0;       
                    ready_out_nxt = 1'b0;
                    overflow_nxt  = 1'b0;
                    res_upper_nxt = 64'b0;
                    res_lower_nxt = 64'b0;
                    res_yw_nxt    = 64'b0;
                    mid_horner_nxt = 32'b0;
                    mid_ywhorner_nxt = 64'b0;
                    xw_nxt = 32'b0;
                    div_a_nxt  = 32'b0;
                    div_b_nxt  = 32'b0;
                    div_greater_nxt = 1'b0;
                end
            endcase
        end
    end
    
endmodule
