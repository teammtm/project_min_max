`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//
// Create Date: 03.09.2020 22:18:45
// Design Name: Project SDUP
// Module Name: minmax
// Project Name: min_max_project
// Target Devices: Zybo
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// coeff - ax + b
// range - [lower, upper]

module degree1(
    input wire clk,
    input wire rst,
    input wire start,
    input wire signed [15:0] a, b, //fixed point 10b.6b
    input wire signed [15:0] lower, upper, //fixed point 10b.6b
    output reg               ready_out,
    output reg               overflow,
    output reg signed [31:0] min, max // fixed point 20b.12b
    );
    
    //states definition
    localparam D1_IDLE     = 3'b000;
    localparam D1_UPPER    = 3'b001;
    localparam D1_LOWER    = 3'b011;
    localparam D1_COMPSEND = 3'b010;
    
    reg signed [31:0] min_nxt, max_nxt;
    reg ready_out_nxt;
    reg overflow_nxt;
    reg [2:0] state_d1, state_d1_nxt;
    wire signed [32:0] b_fix20b12b;
    reg signed [63:0] res_upper, res_upper_nxt, res_lower, res_lower_nxt;
    
    assign b_fix20b12b = {{10{b[15]}},b,6'b0};
    
 //**************FSM******************
    
    //sequential logic
    always @(posedge clk) begin
        if(rst == 1'b1) begin
            state_d1    <= D1_IDLE;
            min         <= 32'b0;
            max         <= 32'b0;
            ready_out   <= 1'b0;
            res_upper   <= 64'b0;
            res_lower   <= 64'b0;
            overflow    <= 1'b0;
        end
        else begin
            state_d1    <= state_d1_nxt;
            min         <= min_nxt;
            max         <= max_nxt;
            ready_out   <= ready_out_nxt;
            res_upper   <= res_upper_nxt;
            res_lower   <= res_lower_nxt;
            overflow    <= overflow_nxt;
        end
    end
    
    // next state logic
    always @* begin 
        if(rst==1'b1) begin
            state_d1_nxt = D1_IDLE;
        end
        else begin
            casex({state_d1,    start})
                  {D1_IDLE,     1'b0 } : state_d1_nxt = D1_IDLE;
                  {D1_IDLE,     1'b1 } : state_d1_nxt = D1_UPPER;
                  {D1_UPPER,    1'bx } : state_d1_nxt = D1_LOWER;
                  {D1_LOWER,    1'bx } : state_d1_nxt = D1_COMPSEND;
                  {D1_COMPSEND, 1'bx } : state_d1_nxt = D1_IDLE;
                  default: state_d1_nxt = state_d1; 
            endcase
        end
    end
    
    //combinational logic
    always @* begin
        if(rst == 1'b1) begin
            min_nxt = 32'b0;       
            max_nxt = 32'b0;       
            ready_out_nxt = 1'b0;
            res_upper_nxt = 64'b0;
            res_lower_nxt = 64'b0;
            overflow_nxt = 1'b0;
        end
        else begin
            min_nxt = 32'b0;       
            max_nxt = 32'b0;       
            ready_out_nxt = 1'b0;
            res_upper_nxt = 64'b0;
            res_lower_nxt = 64'b0;
            overflow_nxt = 1'b0;

            case(state_d1_nxt)
            
                D1_IDLE: begin
                    min_nxt = min;     
                    max_nxt = max;
                    ready_out_nxt = ready_out;
                    overflow_nxt = overflow;
                end
                
                D1_UPPER: begin 
                    res_upper_nxt = (a * upper) + b_fix20b12b;
                end
                
                D1_LOWER: begin
                    res_lower_nxt = (a * lower) + b_fix20b12b;
                    res_upper_nxt = res_upper;
                end
                
                D1_COMPSEND: begin
                    
                    if(res_upper > res_lower) begin
                        max_nxt = res_upper[31:0];
                        min_nxt = res_lower[31:0];
                    end                    
                    else begin
                        max_nxt = res_lower[31:0];
                        min_nxt = res_upper[31:0];
                    end
                    
                    overflow_nxt = ((&res_lower[63:31])^(|res_lower[63:31])) | ((&res_upper[63:31])^(|res_upper[63:31]));
                    ready_out_nxt = 1'b1;                    
                end
                
                default: begin
                    min_nxt = 32'b0;     
                    max_nxt = 32'b0;     
                    ready_out_nxt = 1'b0;
                    res_upper_nxt = 64'b0;
                    res_lower_nxt = 64'b0;
                    overflow_nxt = 1'b0;
                end
            endcase
        end
    end
    
endmodule
