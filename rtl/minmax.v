`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//
// Create Date: 03.09.2020 22:18:45
// Design Name: Project SDUP
// Module Name: minmax
// Project Name: min_max_project
// Target Devices: Zybo
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// coeff - ax^3 + bx^2 + cx + d
// range - [lower, upper]

module minmax(
    input wire clk,
    input wire rst,
    input wire start,
    input wire signed [15:0]  a, b, c, d, //fixed point 10b.6b
    input wire signed [15:0] lower, upper, //fixed point 10b.6b
    output reg               ready_out,
    output reg               overflow,
    output reg signed [31:0] min, max // fixed point 20b.12b
    );
    
    //states definition
    localparam TOP_IDLE     = 2'b00;
    localparam TOP_CMPCOEF  = 2'b01;
    localparam TOP_WAIT     = 2'b10;
    localparam TOP_SEND     = 2'b11;
    
    reg signed [31:0] min_nxt, max_nxt;
    reg ready_out_nxt;
    reg overflow_nxt;
    reg [1:0] state_top, state_top_nxt;
    reg calc_1, calc_1_nxt, calc_2, calc_2_nxt, calc_3, calc_3_nxt;
    reg [1:0] current_degree, current_degree_nxt;
    
    wire result1_ready, result2_ready, result3_ready;
    wire signed [31:0] result1_min, result1_max, result2_min, result2_max, result3_min, result3_max;
    wire calc_ready;
    wire overflow1, overflow2, overflow3;
    
    assign calc_ready = result1_ready | result2_ready | result3_ready;
    
    degree1 u_degree1(
        .clk(clk),
        .rst(rst),
        .a(c),
        .b(d),
        .start(calc_1),
        .lower(lower),
        .upper(upper),
        .min(result1_min),
        .max(result1_max),
        .overflow(overflow1),
        .ready_out(result1_ready)
    );
    
    degree2 u_degree2(
        .clk(clk),
        .rst(rst),
        .a(b),
        .b(c),
        .c(d),
        .start(calc_2),
        .lower(lower),
        .upper(upper),
        .min(result2_min),
        .max(result2_max),
        .overflow(overflow2),
        .ready_out(result2_ready)
    );
        
    degree3 u_degree3(
        .clk(clk),
        .rst(rst),
        .a(a),
        .b(b),
        .c(c),
        .d(d),
        .start(calc_3),
        .lower(lower),
        .upper(upper),
        .min(result3_min),
        .max(result3_max),
        .overflow(overflow3),
        .ready_out(result3_ready)
    );
    
 //**************FSM******************
    
    //sequential logic
    always @(posedge clk) begin
        #1
        if(rst == 1'b1) begin
            state_top   <= TOP_IDLE;
            min         <= 32'b0;
            max         <= 32'b0;
            ready_out   <= 1'b0;
            calc_1      <= 1'b0;
            calc_2      <= 1'b0;
            calc_3      <= 1'b0;
            current_degree <= 2'b0;
            overflow    <= 1'b0;
        end
        else begin
            state_top   <= state_top_nxt;
            min         <= min_nxt;
            max         <= max_nxt;
            ready_out   <= ready_out_nxt;
            overflow    <= overflow_nxt;
            calc_1      <= calc_1_nxt;
            calc_2      <= calc_2_nxt;
            calc_3      <= calc_3_nxt;
            current_degree <= current_degree_nxt;
        end
    end
    
    // next state logic
    always @* begin 
        if(rst==1'b1) begin
            state_top_nxt = TOP_IDLE;
        end
        else begin
            casex({state_top,   start, current_degree, result1_ready, result2_ready, result3_ready})
                  {TOP_IDLE,    1'b0,  2'bxx,           1'bx,          1'bx,          1'bx         } : state_top_nxt = TOP_IDLE;
                  {TOP_IDLE,    1'b1,  2'bxx,           1'bx,          1'bx,          1'bx         } : state_top_nxt = TOP_CMPCOEF;
                  {TOP_CMPCOEF, 1'bx,  2'bxx,           1'bx,          1'bx,          1'bx         } : state_top_nxt = TOP_WAIT;
                  {TOP_WAIT,    1'bx,  2'b00,           1'bx,          1'bx,          1'bx         } : state_top_nxt = TOP_SEND;
                  {TOP_WAIT,    1'bx,  2'b01,           1'b1,          1'bx,          1'bx         } : state_top_nxt = TOP_SEND;
                  {TOP_WAIT,    1'bx,  2'b01,           1'b0,          1'bx,          1'bx         } : state_top_nxt = TOP_WAIT;
                  {TOP_WAIT,    1'bx,  2'b10,           1'bx,          1'b1,          1'bx         } : state_top_nxt = TOP_SEND;
                  {TOP_WAIT,    1'bx,  2'b10,           1'bx,          1'b0,          1'bx         } : state_top_nxt = TOP_WAIT;
                  {TOP_WAIT,    1'bx,  2'b11,           1'bx,          1'bx,          1'b1         } : state_top_nxt = TOP_SEND;
                  {TOP_WAIT,    1'bx,  2'b11,           1'bx,          1'bx,          1'b0         } : state_top_nxt = TOP_WAIT;
                  {TOP_SEND,    1'b1,  2'bxx,           1'bx,          1'bx,          1'bx         } : state_top_nxt = TOP_SEND;
                  {TOP_SEND,    1'b0,  2'bxx,           1'bx,          1'bx,          1'bx         } : state_top_nxt = TOP_IDLE;
                  default: state_top_nxt = state_top; 
            endcase
        end
    end
    
    //combinational logic
    always @* begin
        if(rst == 1'b1) begin
            min_nxt = 32'b0;       
            max_nxt = 32'b0;       
            ready_out_nxt = 1'b0;
            {calc_3_nxt,calc_2_nxt,calc_1_nxt} = 3'b0;
            current_degree_nxt = 2'b0;
            overflow_nxt = 1'b0;
        end
        else begin
            min_nxt = 32'b0;       
            max_nxt = 32'b0;       
            ready_out_nxt = 1'b0;
            {calc_3_nxt,calc_2_nxt,calc_1_nxt} = 3'b0;
            current_degree_nxt = 2'b0;
            overflow_nxt = 1'b0;
            
            case(state_top_nxt)
            
                TOP_IDLE: begin
                    min_nxt = min;     
                    max_nxt = max;
                    ready_out_nxt = ready_out;
                    overflow_nxt = overflow;
                end
                
                TOP_CMPCOEF: begin
                    if(a == 8'b0 && b == 8'b0 && c == 8'b0) begin
                        min_nxt = {{10{d[15]}},d,6'b0}; 
                        max_nxt = {{10{d[15]}},d,6'b0};
                        {calc_3_nxt,calc_2_nxt,calc_1_nxt} = 3'b000;
                         current_degree_nxt = 2'b00;
                    end
                    else if(a == 8'b0 && b == 8'b0) begin
                        {calc_3_nxt,calc_2_nxt,calc_1_nxt} = 3'b001;
                         current_degree_nxt = 2'b01;
                    end
                    else if(a == 8'b0) begin
                        {calc_3_nxt,calc_2_nxt,calc_1_nxt} = 3'b010;
                         current_degree_nxt = 2'b10;
                    end
                    else begin
                        {calc_3_nxt,calc_2_nxt,calc_1_nxt} = 3'b100;
                         current_degree_nxt = 2'b11;
                    end
                end
                
                TOP_WAIT: begin
                    {calc_3_nxt,calc_2_nxt,calc_1_nxt} = {calc_3,calc_2,calc_1};
                    current_degree_nxt = current_degree;
                    min_nxt = min;     
                    max_nxt = max; 
                end
                
                TOP_SEND: begin
                    ready_out_nxt = 1'b1;
                    if({calc_3,calc_2,calc_1} == 3'b000) begin
                        min_nxt = min;
                        max_nxt = max;
                        overflow_nxt = 1'b0;
                    end
                    else if({calc_3,calc_2,calc_1} == 3'b001) begin
                        min_nxt = result1_min;
                        max_nxt = result1_max;
                        overflow_nxt = overflow1;
                    end
                    else if({calc_3,calc_2,calc_1} == 3'b010) begin
                        min_nxt = result2_min;
                        max_nxt = result2_max;
                        overflow_nxt = overflow2;
                    end
                    else if({calc_3,calc_2,calc_1} == 3'b100) begin
                        min_nxt = result3_min;
                        max_nxt = result3_max;
                        overflow_nxt = overflow3;
                    end
                end
                
                default: begin
                    min_nxt = 32'b0;     
                    max_nxt = 32'b0;     
                    ready_out_nxt = 1'b0;
                    {calc_3_nxt,calc_2_nxt,calc_1_nxt} = 3'b000;
                    overflow_nxt = 1'b0;
                end
            endcase
        end
    end
    
endmodule
