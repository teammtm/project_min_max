clear all; close all; clc

file = fopen('testvectors_degree1.tv','w');

for i = 1:100
    
    a = randi([-511,511],1);
    b = randi([-511,511],1);
    
    lower =  randi([-511,511],1);
    upper =  randi([-511,511],1);
    
    lower = lower / 64;
    upper = upper / 64;
    
    
    while (lower > upper)
        lower =  randi([-511,511],1);
        upper =  randi([-511,511],1);
        
         lower = lower / 64;
         upper = upper / 64;
    end
    
    x = a*lower+b;
    y = a*upper+b;
    
    
    
     if y < x
        min = y;
        max = x;
    else
        min = x;
        max = y;
     end
    
     fprintf('%fx+%f low: %f up: %f min: %f max: %f\n', a, b, lower, upper, min, max);
    
    min = fi(min, 1, 32, 12);
    max = fi(max, 1, 32, 12);
    
    lowerb = fi(lower, 1, 16, 6);
    upperb = fi(upper, 1, 16, 6);
    ab = fi(a, 1, 16, 6);
    bb = fi(b, 1, 16, 6);
    
    minb = bin(min);
    maxb = bin(max);   
    
    lowerb = bin(lowerb);
    upperb = bin(upperb);
    ab = bin(ab);
    bb = bin(bb);
    
    fprintf(file,'%s_%s_%s_%s_%s_%s\n', ab, bb, lowerb, upperb, minb, maxb);
end


fclose(file);