`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.09.2020 22:35:52
// Design Name: 
// Module Name: degree1_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module degree1_tb;

    reg clk;
    reg rst;
    reg start;
    reg signed [15:0] a, b; //fixed point 10b.6b
    reg signed [15:0] lower, upper; //fixed point 10b.6b
    reg signed [31:0] min_expected, max_expected;
    wire ready_out;
    wire overflow1;
    wire signed [31:0] min, max; // fixed point 20b.12b
    
    reg[127:0] read_data [0:99];
    integer i;
    
    degree1 u_degree1(
            .clk(clk),
            .rst(rst),
            .a(a),
            .b(b),
            .start(start),
            .lower(lower),
            .upper(upper),
            .min(min),
            .max(max),
            .overflow(overflow1),
            .ready_out(ready_out)
        );
    
    
    localparam period = 20; 
    initial
    begin
        rst <= 1'b1;
        #100 rst <= 1'b0;
    end
    
   
    initial
    begin
        clk <= 1'b1;
       
    end
    
    always begin
        #5 clk <= ~clk;
    end
    
    
    initial begin
         start <= 1'b0;
         $readmemb("testvectors_degree1.tv", read_data);
         
         for (i=0; i<100; i=i+1) begin
            #300
            {a, b, lower, upper, min_expected, max_expected} <= read_data[i];
            #105
            start <= 1'b1;
            #20
            start <= 1'b0;
            
            wait (ready_out == 1'b1) begin
                if ((min == min_expected) && (max == max_expected)) begin
                    $display("PASS");
                end else begin
                    $display("FAIL:  min: %b, min_exp: %b, max: %b, max_exp: %b" , min, min_expected, max, max_expected);  
                end 
            end
           
           
           
            
        end
    
    end

   
endmodule
