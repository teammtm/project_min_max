clear all; close all; clc

file = fopen('testvectors_degree3.tv','w');

for i = 1:10
    
    a = randi([-100,100],1);
    b = randi([-100,100],1);
    c = randi([-100,100],1);
    d = randi([-100,100],1);
    
    lower =  randi([-100,100],1);
    upper =  randi([-100,100],1);
    
    lower = lower / 64;
    upper = upper / 64;
    
    
    while (lower > upper)
        lower =  randi([-100,100],1);
        upper =  randi([-100,100],1);
        
       lower = lower / 64;
      upper = upper / 64;
    end
    
       % krance przedzialu (schemat Hornera)
    yk = ( ( (a*lower) + b )*lower + c )*lower +d;
    yl = ( ( (a*upper) + b )*upper + c )*upper +d;
    
    if yk < yl
        min = yk;
        max = yl;
    else
        min = yl;
        max = yk;
    end
    
 
    %pochodna
    poch0 = c;
    poch = [b*2 a*3];
    
    %wierzcholek
    % x wierzcholka xw = -b/2a - dzielenie w verilogu przez odejmowanie 
    %(skalowanie przez przesuniecia w systemie binarnym) 
    tmp_a = abs(poch(1)*4096);
    tmp_b = abs(poch(2)+poch(2));
    xw = 0;
    while tmp_a >= tmp_b
        tmp_a = tmp_a - tmp_b;
        xw = xw + 1;
    end
    if((poch(1)<0 && poch(2)<0) || (poch(1)>0 && poch(2)>0))
    xw = -xw / 4096;
    else
    xw = xw / 4096;
    end
    
    % jesli jest to funkcja kwadratowa od wierzcholka w lewo i od wierzcholka w prawo jest monotoniczna,
    % czyli sa stale rosnace/malejace przedzialy [k xw] oraz [xw l]
    % mozna wyszukac miejsca zerowe metoda bisekcji
    %wartosc wierzcholka
    yw = ( (poch(2)*xw) + poch(1) )*xw + poch0;
    yk_p = ( (poch(2)*lower) + poch(1) )*lower + poch0;
    yl_p = ( (poch(2)*upper) + poch(1) )*upper + poch0;
    % jesli wartosc ma ten sam znak co ktorys z koncow przedzialu to w
    % podprzedziale [xw ten_koniec] nie ma miejsca zerowego
    % jesli wierzcholek ma wartosc 0 to jest jedynym miejscem zerowym
    if(yw == 0 && (xw>lower && xw<l))
        yeks = ( ( (a*xw) + b )*xw + c )*xw +d;
        if yeks < min
            min = yeks;
        end
        if yeks > max
            max = yeks;
        end
    elseif((yw > 0 && poch(2) < 0) || (yw < 0 && poch(2) > 0))
        if(((yw > 0 && yk_p < 0) || (yw < 0 && yk_p > 0))&&((yw > 0 && yl_p < 0) || (yw < 0 && yl_p > 0)))
            temp_k = lower;
            temp_yk = yk_p;
            temp_l = xw;
            temp_yl = yw;
            en=1;
            viet=1;
        elseif((yw > 0 && yk_p < 0) || (yw < 0 && yk_p > 0))
            temp_k = lower;
            temp_yk = yk_p;
            temp_l = xw;
            temp_yl = yw;
            en=1;
            viet=0;
        elseif((yw > 0 && yl_p < 0) || (yw < 0 && yl_p > 0))
            temp_k = xw;
            temp_yk = yw;
            temp_l = upper;
            temp_yl = yl_p;
            en=1;
            viet = 0;
        else
            en =0;
            viet = 0;
        end
        if (en==1)
            for i=1:32
               xz1 = (temp_k+temp_l)/2;
               yz1 = ( (poch(2)*xz1) + poch(1) )*xz1 + poch0;

               if((yz1<0 && temp_yk>0) || (yz1>0 && temp_yk<0))
                   temp_l = xz1;
                   temp_yl = yz1;
               elseif((yz1<0 && temp_yl>0) || (yz1>0 && temp_yl<0))
                   temp_k = xz1;
                   temp_yk = yz1;
               end

            end
            if(xz1>lower && xz1<upper)
                yeks1 = ( ( (a*xz1) + b )*xz1 + c )*xz1 +d;
                if yeks1 < min
                    min = yeks1;
                end
                if yeks1 > max
                    max = yeks1;
                end
            end

        end
        if(viet ==1)
            xz2 = xw*2 - xz1;
            if(xz2>lower && xz2<upper)
                yeks2 = ( ( (a*xz2) + b )*xz2+ c )*xz2 +d;
                if yeks2 < min
                    min = yeks2;
                end
                if yeks2 > max
                    max = yeks2;
                end
            end
        end
    end
   
   
     fprintf( '%fx^3+%fx^2+%fx+%f low: %f up: %f min: %f max: %f\n', a, b, c, d,  lower, upper, min, max);
    
    min = fi(min, 1, 32, 12);
    max = fi(max, 1, 32, 12);
    
    lowerb = fi(lower, 1, 16, 6);
    upperb = fi(upper, 1, 16, 6);
    ab = fi(a, 1, 16, 6);
    bb = fi(b, 1, 16, 6);
    cb = fi(c, 1, 16, 6);
    db = fi(d, 1, 16, 6);
    
    minb = bin(min);
    maxb = bin(max);   
    
    lowerb = bin(lowerb);
    upperb = bin(upperb);
    ab = bin(ab);
    bb = bin(bb);
    cb = bin(cb);
    db = bin(db);
    
    fprintf(file,'%s_%s_%s_%s_%s_%s_%s_%s\n', ab, bb, cb, db, lowerb, upperb, minb, maxb);
end


fclose(file);
