clear all; close all; clc

file = fopen('testvectors_degree24.tv','w');

for i = 1:10
    
    a = randi([-10,50],1);
    b = randi([-10,50],1);
    c = randi([-10,50],1);
    
    lower =  randi([-10,50],1);
    upper =  randi([-10,50],1);
    
    lower = lower / 64;
    upper = upper / 64;
    
    
    while (lower > upper)
        lower =  randi([-10,50],1);
        upper =  randi([-10,50],1);
        
       lower = lower / 64;
      upper = upper / 64;
    end
    
 % krance przedzialu (schemat Hornera)
    yk = ( (a*lower) + b )*lower + c;
    yl = ( (a*upper) + b )*upper + c;
    
    if yk < yl
        min = yk;
        max = yl;
    else
        min = yl;
        max = yk;
    end
    
    % x wierzcholka xw = -b/2a - dzielenie w verilogu przez odejmowanie, 
    % (skalowanie przez przesuniecia w systemie binarnym)
    tmp_a = abs(b*4096);
    tmp_b = abs(a+a);
    xw = 0;
    while tmp_a >= tmp_b
        tmp_a = tmp_a - tmp_b;
        xw = xw + 1;
    end
    
    if((b<0 && a<0) || (b>0 && a>0))
    xw = -xw / 4096;
    else
    xw = xw / 4096;
    end
    
    %jesli xw jest w przedziale [k l] to obliczyc wartosc
    if xw>lower && xw<upper
        yw = ( (a*xw) + b )*xw + c;
        
        if yw < min
            min = yw;
        end
        if yw > max
            max = yw;
        end
    end
    
   
     fprintf('%fx^2+%fx+%f low: %f up: %f min: %f max: %f\n', a, b, c, lower, upper, min, max);
    
    min = fi(min, 1, 32, 12);
    max = fi(max, 1, 32, 12);
    
    lowerb = fi(lower, 1, 16, 6);
    upperb = fi(upper, 1, 16, 6);
    ab = fi(a, 1, 16, 6);
    bb = fi(b, 1, 16, 6);
    cb = fi(c, 1, 16, 6);
    
    minb = bin(min);
    maxb = bin(max);   
    
    lowerb = bin(lowerb);
    upperb = bin(upperb);
    ab = bin(ab);
    bb = bin(bb);
    cb = bin(cb);
    
    fprintf(file,'%s_%s_%s_%s_%s_%s_%s\n', ab, bb, cb, lowerb, upperb, minb, maxb);
end


fclose(file);