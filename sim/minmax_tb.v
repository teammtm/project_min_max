`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06.09.2020 19:38:13
// Design Name: 
// Module Name: degree3_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module minmax_tb();
 reg clk;
   reg rst;
   reg start;
   reg signed [15:0] a, b, c, d; //fixed point 10b.6b
   reg signed [15:0] lower, upper; //fixed point 10b.6b
   reg signed [31:0] min_expected, max_expected;
   wire ready_out;
   wire overflow;
   wire signed [31:0] min, max; // fixed point 20b.12b

   reg[159:0] read_data [0:29];
   integer i, signmin, signmax;
   
   real minr, maxr, minExp, maxExp;

   minmax UUT(
           .clk(clk),
           .rst(rst),
           .a(a),
           .b(b),
           .c(c),
           .d(d),
           .start(start),
           .lower(lower),
           .upper(upper),
           .min(min),
           .max(max),
           .overflow(overflow),
           .ready_out(ready_out)
       );



   localparam period = 20; 
   initial
   begin
       rst <= 1'b1;
       #100 rst <= 1'b0;
   end
   
  
   initial
   begin
       clk <= 1'b1;
      
   end
   
   always begin
       #5 clk <= ~clk;
   end
   
   
   initial begin
        start <= 1'b0;
        $readmemb("testvectors_minmax.tv", read_data);
        
        for (i=0; i<30; i=i+1) begin
           #300
           {a, b, c, d, lower, upper, min_expected, max_expected} <= read_data[i];
           #105
           start <= 1'b1;
           #20
           start <= 1'b0;
           
           
           
           wait (ready_out == 1'b1) begin
           
                   minr = min;
                   minr = minr/4096;
                   
                   maxr = max;
                   maxr = maxr/4096;
                   
                   maxExp = max_expected;
                   maxExp = maxExp/4096;
                    
                   minExp = min_expected;
                   minExp = minExp/4096;
                   
                    if (minr < 0 ) signmin = -1;
                    else signmin = 1;
                    
                    if (maxr < 0 ) signmax = -1;
                    else signmax = 1;
                                      
               if ((( minr*signmin > (0.98*minExp*signmin) )&&(  minr*signmin < (1.02*minExp*signmin)  )) && (( maxr*signmax > (0.98*maxExp*signmax) )&&(  maxr*signmax < (1.02*maxExp*signmax)  ))) begin
                   $display("PASS");
               end else begin
                   
                   $display("FAIL:  min: %f, min_exp: %f, max: %f, max_exp: %f" , minr, minExp, maxr, maxExp);  
               end 
           end
          
          
          
           
       end
   
   end
    
endmodule
