/***************************** Include Files *********************************/
#include "xil_io.h"
#include "xparameters.h"
#include "min_maxip.h"

/**************************** user definitions ********************************/

//Minmax processor base addres redefinition
#define MINMAX_BASE_ADDR      XPAR_MIN_MAXIP_0_S00_AXI_BASEADDR
//Minmax processor registers' offset redefinition
#define CONTROL_REG_OFFSET    MIN_MAXIP_S00_AXI_SLV_REG0_OFFSET
#define COEFF_A_B_REG_OFFSET      MIN_MAXIP_S00_AXI_SLV_REG1_OFFSET
#define COEFF_C_D_REG_OFFSET      MIN_MAXIP_S00_AXI_SLV_REG2_OFFSET
#define RANGE_REG_OFFSET      MIN_MAXIP_S00_AXI_SLV_REG3_OFFSET
#define STATUS_REG_OFFSET     MIN_MAXIP_S00_AXI_SLV_REG4_OFFSET
#define RESULT_MIN_REG_OFFSET    MIN_MAXIP_S00_AXI_SLV_REG5_OFFSET
#define RESULT_MAX_REG_OFFSET    MIN_MAXIP_S00_AXI_SLV_REG6_OFFSET

//Minmax processor bits masks
#define CONTROL_REG_START_MASK (u32)(0x01)
#define STATUS_REG_READY_MASK (u32)(0x01)

// Macors to extract overflow and ready values from the output data register
// Shift left and right to fill msb of int32_t with ones - arithmetic shift  
#define STATUS_REG_READY(param)  ((((s32)param & (s32)0x00000001)<<31)>>31)
#define STATUS_REG_OVERFLOW(param)  ((((s32)param & (s32)0x00000002)<< 30)>>31)


/***************************** calculateCordicVal function **********************
* The function runs the Minmax accelerator IP
* Argument:
* a, b, c, d - input coef . Fixed-point(10:6) format
* lower, upper - input range . Fixed-point(10:6) format
* Return values:
* min - minimum value. Fixed-point(20:12) format
* max - maximum value. Fixed-point(20:12) format
*
*/

int calculateMinMax(s16 a, s16 b, s16 c, s16 d, s16 lower, s16 upper, u16* overflow, s32* min,  s32* max)
{


u32 coeff1  = (b<<16) | (0x0000FFFF & a);

u32 coeff2  = (d<<16) | (0x0000FFFF & c);

u32 range  = (upper<<16) | (0x0000FFFF & lower);

u32 status;
s32 result_min;
s32 result_max;

//Send data to data register of Minmax processor
MIN_MAXIP_mWriteReg(MINMAX_BASE_ADDR, COEFF_A_B_REG_OFFSET, coeff1);
MIN_MAXIP_mWriteReg(MINMAX_BASE_ADDR, COEFF_C_D_REG_OFFSET, coeff2);
MIN_MAXIP_mWriteReg(MINMAX_BASE_ADDR, RANGE_REG_OFFSET , range);

//Start Minmax processor - pulse start bit in control register
MIN_MAXIP_mWriteReg(MINMAX_BASE_ADDR, CONTROL_REG_OFFSET, CONTROL_REG_START_MASK);
MIN_MAXIP_mWriteReg(MINMAX_BASE_ADDR, CONTROL_REG_OFFSET, 0);
//Wait for ready bit in status register
	while( (MIN_MAXIP_mReadReg(MINMAX_BASE_ADDR, STATUS_REG_OFFSET) & STATUS_REG_READY_MASK) == 0);

//Get results
	status = MIN_MAXIP_mReadReg(MINMAX_BASE_ADDR, STATUS_REG_OFFSET);
	result_min = MIN_MAXIP_mReadReg(MINMAX_BASE_ADDR, RESULT_MIN_REG_OFFSET);
	result_max = MIN_MAXIP_mReadReg(MINMAX_BASE_ADDR, RESULT_MAX_REG_OFFSET);

//Extract overflow from 32-bit register data, read min and max
	*overflow = (status & 0x0000002)>>1;
	*min = result_min;
	*max = result_max;
	
	return 1;
}
