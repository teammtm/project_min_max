/*
 * main.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"


/**
 *
 */
int calculateMinMax(s16 a, s16 b, s16 c, s16 d, s16 lower, s16 upper, u16* overflow, s32* min,  s32* max);


/**
 *
 */
s32 read10b6bfixVal(){
s32 int_val = 0;
s32 frac_val = 0;
s32 ret = 0;
s32 sign = 0;
char8 c;
    outbyte ( c = inbyte() );
    if (c == '-'){
    	sign = -1;
        outbyte ( c = inbyte() );
        int_val += 100 * (c - '0');
    }
    else{
    	sign = 1;
    	int_val += 100 * (c - '0');
    }
    outbyte ( c = inbyte() );
    int_val += 10 * (c - '0');
    outbyte ( c = inbyte() );
    int_val += (c - '0');
    outbyte('.');

    outbyte ( c = inbyte() );
    frac_val += 10 * (c - '0');
    outbyte ( c = inbyte() );
    frac_val += (c - '0');

    ret = sign *(((int_val*100)+frac_val)<<6)/100;
    print("\n\r");
    //xil_printf("%u", ret);
    //print("\n\r");

    return ret;
}

/**
 *  printDecimalFXPVal - print fixed-point value in decimal format
 *  val - value to print out in radix-2 fixed-point
 *  scale - Fixed-point scaling factor
 *	nbr_of_decimal_digit - number precision. The number of digits after decimal point
 */

void printDecimalFXPVal(s32 val, u32 scale, u8 nbr_of_decimal_digit ){
	u32 i;
	//Change radix 2 to radix 10 fixed-point. Spare one more decimal point for rounding
	for( i=0; i<nbr_of_decimal_digit; i++ ) val=val*10; //Multiply by 10^nbr_of_decimal_digit+1
	val /= (s32) scale;
	//Round target fixed-point to nearst integer
	//val = (val +5 )/10;

	xil_printf("%dE-%u", val, nbr_of_decimal_digit );
}

int main()
{
	s16 a = 0;
	s16 b = 0;
	s16 c = 0;
	s16 d = 0;
	s16 lower = 0;
	s16 upper = 0;
	u16 overflow = 0;
	s32 min = 0;
	s32 max = 0;


    init_platform();

    while(1){
    	print("Enter A = ");
    	a = read10b6bfixVal();
    	print("Enter B = ");
    	b = read10b6bfixVal();
    	print("Enter C = ");
    	c = read10b6bfixVal();
    	print("Enter D = ");
    	d = read10b6bfixVal();
    	print("Enter lower = ");
    	lower = read10b6bfixVal();
    	print("Enter upper = ");
    	upper = read10b6bfixVal();
    	print("\n\r");
    	//Convert to fxp value(10:6)

    	calculateMinMax(a, b, c, d, lower, upper, &overflow, &min,  &max);

    	print("min ");
    	printDecimalFXPVal(min, 4096, 2);
    	print("\n\r");
    	//xil_printf("%u", min);
    	//print("\n\r");
    	print("max ");
    	printDecimalFXPVal(max, 4096, 2);
    	print("\n\r");
    	//xil_printf("%u", max);
    	//print("\n\r");
    	if(overflow == 1){
    		print("overflow occurred");
    	}
    	print("\n\r");
    	print("\n\r");
    }

}
