clear all; close all; clc
%% minmax wielomianu stopnia 3 lub mniejszego o zadanych wspolczynnikach (calkowitych) w zadanym przedziale
% wielomian - p(x) = a0 + a1*x + a2*x^2 + a3*x^3
% przedzial - [k,l]

a0 = 4;
a = [-3 -4 3];

k = -0.75;
l = 1.25;

%% 
if(a(1)==0 && a(2)==0 && a(3)==0)
    minval = a0;
    maxval = a0;
    
elseif (a(2)==0 && a(3)==0)
    % tylko krance przedzialu
    yk = k*a(1)+a0;
    yl = l*a(1)+a0;
    
    if yk < yl
        minval = yk;
        maxval = yl;
    else
        minval = yl;
        maxval = yk;
    end

elseif (a(3)==0)
    % krance przedzialu (schemat Hornera)
    yk = ( (a(2)*k) + a(1) )*k + a0;
    yl = ( (a(2)*l) + a(1) )*l + a0;
    
    if yk < yl
        minval = yk;
        maxval = yl;
    else
        minval = yl;
        maxval = yk;
    end
    
    % x wierzcholka xw = -b/2a - dzielenie w verilogu przez odejmowanie, 
    % (skalowanie przez przesuniecia w systemie binarnym)
    tmp_a = abs(a(1)*4096);
    tmp_b = abs(a(2)+a(2));
    xw = 0;
    while tmp_a >= tmp_b
        tmp_a = tmp_a - tmp_b;
        xw = xw + 1;
    end
    
    if((a(1)<0 && a(2)<0) || (a(1)>0 && a(2)>0))
    xw = -xw / 4096;
    else
    xw = xw / 4096;
    end
    
    %jesli xw jest w przedziale [k l] to obliczyc wartosc
    if xw>k && xw<l
        yw = ( (a(2)*xw) + a(1) )*xw + a0;
        
        if yw < minval
            minval = yw;
        end
        if yw > maxval
            maxval = yw;
        end
    end
    
else
    % krance przedzialu (schemat Hornera)
    yk = ( ( (a(3)*k) + a(2) )*k + a(1) )*k +a0;
    yl = ( ( (a(3)*l) + a(2) )*l + a(1) )*l +a0;
    
    if yk < yl
        minval = yk;
        maxval = yl;
    else
        minval = yl;
        maxval = yk;
    end
    
    %pochodna
    poch0 = a(1);
    poch = [a(2)*2 a(3)*3];
    
    %wierzcholek
    % x wierzcholka xw = -b/2a - dzielenie w verilogu przez odejmowanie 
    %(skalowanie przez przesuniecia w systemie binarnym) 
    tmp_a = abs(poch(1)*4096);
    tmp_b = abs(poch(2)+poch(2));
    xw = 0;
    while tmp_a >= tmp_b
        tmp_a = tmp_a - tmp_b;
        xw = xw + 1;
    end
    if((poch(1)<0 && poch(2)<0) || (poch(1)>0 && poch(2)>0))
    xw = -xw / 4096;
    else
    xw = xw / 4096;
    end
    
    % jesli jest to funkcja kwadratowa od wierzcholka w lewo i od wierzcholka w prawo jest monotoniczna,
    % czyli sa stale rosnace/malejace przedzialy [k xw] oraz [xw l]
    % mozna wyszukac miejsca zerowe metoda bisekcji
    %wartosc wierzcholka
    yw = ( (poch(2)*xw) + poch(1) )*xw + poch0;
    yk_p = ( (poch(2)*k) + poch(1) )*k + poch0;
    yl_p = ( (poch(2)*l) + poch(1) )*l + poch0;
    % jesli wartosc ma ten sam znak co ktorys z koncow przedzialu to w
    % podprzedziale [xw ten_koniec] nie ma miejsca zerowego
    % jesli wierzcholek ma wartosc 0 to jest jedynym miejscem zerowym
    if(yw == 0 && (xw>k && xw<l))
        yeks = ( ( (a(3)*xw) + a(2) )*xw + a(1) )*xw +a0;
        if yeks < minval
            minval = yeks;
        end
        if yeks > maxval
            maxval = yeks;
        end
    elseif((yw > 0 && poch(2) < 0) || (yw < 0 && poch(2) > 0))
        if(((yw > 0 && yk_p < 0) || (yw < 0 && yk_p > 0))&&((yw > 0 && yl_p < 0) || (yw < 0 && yl_p > 0)))
            temp_k = k;
            temp_yk = yk_p;
            temp_l = xw;
            temp_yl = yw;
            en=1;
            viet=1;
        elseif((yw > 0 && yk_p < 0) || (yw < 0 && yk_p > 0))
            temp_k = k;
            temp_yk = yk_p;
            temp_l = xw;
            temp_yl = yw;
            en=1;
            viet=0;
        elseif((yw > 0 && yl_p < 0) || (yw < 0 && yl_p > 0))
            temp_k = xw;
            temp_yk = yw;
            temp_l = l;
            temp_yl = yl_p;
            en=1;
            viet = 0;
        else
            en =0;
            viet = 0;
        end
        if (en==1)
            for i=1:32
               xz1 = (temp_k+temp_l)/2;
               yz1 = ( (poch(2)*xz1) + poch(1) )*xz1 + poch0;

               if((yz1<0 && temp_yk>0) || (yz1>0 && temp_yk<0))
                   temp_l = xz1;
                   temp_yl = yz1;
               elseif((yz1<0 && temp_yl>0) || (yz1>0 && temp_yl<0))
                   temp_k = xz1;
                   temp_yk = yz1;
               end

            end
            if(xz1>k && xz1<l)
                yeks1 = ( ( (a(3)*xz1) + a(2) )*xz1 + a(1) )*xz1 +a0;
                if yeks1 < minval
                    minval = yeks1;
                end
                if yeks1 > maxval
                    maxval = yeks1;
                end
            end

        end
        if(viet ==1)
            xz2 = xw*2 - xz1;
            if(xz2>k && xz2<l)
                yeks2 = ( ( (a(3)*xz2) + a(2) )*xz2+ a(1) )*xz2 +a0;
                if yeks2 < minval
                    minval = yeks2;
                end
                if yeks2 > maxval
                    maxval = yeks2;
                end
            end
        end
    end
end

minval
maxval

